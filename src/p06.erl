-module(p06).

-include("tests_inc.hrl").

-export([is_palindrome/1]).

is_palindrome(Xs) ->
    Xs =:= p05:reverse(Xs).

-ifdef(TEST).

is_palindrome_test_() -> [
    {"is_palindrome(42) throws exception",
        ?_assertException(error, function_clause, is_palindrome(42))},
    {"[] is palindrome",
        ?_assert(is_palindrome([]))},
    {"[1,2,3,2,1] is palindrome",
        ?_assert(is_palindrome([1,2,3,2,1]))},
    {"[1, 2, 1] is palindrome",
        ?_assert(is_palindrome([1, 2, 1]))},
    {"[a, b, b, a] is palindrome",
        ?_assert(is_palindrome([a, b, b, a]))},
    {"[1, 2] is not palindrome",
        ?_assertNot(is_palindrome([1, 2]))},
    {"[a, b, a, b] is not palindrome",
        ?_assertNot(is_palindrome([a, b, a, b]))}
].

-endif.
