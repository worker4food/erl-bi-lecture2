-module(p01).

-include("tests_inc.hrl").

-export([last/1]).

last([X]) ->
    X;
last([_ | T]) ->
    last(T).

-ifdef(TEST).

last_test_() -> [
    {"last(42) throws exception",
        ?_assertException(error, function_clause, last(42))},
    {"last([]) throws exception",
        ?_assertException(error, function_clause, last([]))},
    {"last([a,b,c,d,e,f]) is f",
        ?_assertEqual(f, last([a,b,c,d,e,f]))},
    {"last of lists:seq(1, 100_000) is 100_000",
        ?_assertEqual(100000, last(lists:seq(1, 100000)))},
    {"forall Xs, Xs =/=[]. p01:last(Xs) =:= lists:last(Xs)",
        ?_assert(proper:quickcheck(prop_eq_lists_last()))}
].

prop_eq_lists_last() ->
    ?FORALL(Xs, non_empty(list(any())), last(Xs) =:= lists:last(Xs)).

-endif.
