-module(p07).

-include("tests_inc.hrl").

-export([flatten/1]).

flatten(Xs) ->
    R = flatten(Xs, []),
    p05:reverse(R).

flatten([[_ | _] = X | Xs], Acc) ->
    flatten(Xs, flatten(X, Acc));
flatten([[] | Xs], Acc) ->
    flatten(Xs, Acc);
flatten([X | Xs], Acc) ->
    flatten(Xs, [X | Acc]);
flatten([], Acc) ->
    Acc.

-ifdef(TEST).
flatten_test_() -> [
    {"flatten(42) throws exception",
        ?_assertException(error, function_clause, flatten(42))},
    {"flatten([]) is []",
        ?_assertEqual([], flatten([]))},
    {"flatten([a,[],[b,[c,d],e]]) is [a, b, c, d, e]",
        ?_assertEqual([a, b, c, d, e], flatten([a,[],[b,[c,d],e]]))},
    {"forall Xs. flatten(Xs) =:= lists:flatten(Xs)",
        ?_assert(proper:quickcheck(prop_eq_lists_flatten()))}
].

prop_eq_lists_flatten() ->
    ?FORALL(Xs, nested_list(any()), flatten(Xs) =:= lists:flatten(Xs)).

nested_list(Ty) ->
    ?SIZED(N, nested_list(N, Ty)).

nested_list(0, _) ->
    [];
nested_list(Sz, Ty) ->
    ?LAZY(oneof([
        [Ty | nested_list(Sz - 1, Ty)],
        [nested_list(Sz - 1, Ty)]
    ])).
-endif.
