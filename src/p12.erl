-module(p12).

-include("tests_inc.hrl").

-export([decode_modified/1]).

decode_modified(Xs) ->
    decode_modified(Xs, []).

decode_modified([{1, X} | Xs], Acc) ->
    decode_modified(Xs, [X | Acc]);
decode_modified([{N, X} | Xs], Acc) ->
    decode_modified([{N - 1, X} | Xs], [X | Acc]);
decode_modified([X | Xs], Acc) ->
    decode_modified(Xs, [X | Acc]);
decode_modified([], Acc) ->
    p05:reverse(Acc).

-ifdef(TEST).

decode_modified_test_() -> [
    {"decode_modified(42) throws exception",
        ?_assertException(error, function_clause, decode_modified(42))},
    {"decode_modified([]) is []",
        ?_assertEqual([], decode_modified([]))},
    {"decode_modified([{4,a},b,{2,c},{2,a},d,{4,e}]) is [a,a,a,a,b,c,c,a,a,d,e,e,e,e]",
        ?_assertEqual([a,a,a,a,b,c,c,a,a,d,e,e,e,e],
            decode_modified([{4,a},b,{2,c},{2,a},d,{4,e}]))}
].

-endif.
