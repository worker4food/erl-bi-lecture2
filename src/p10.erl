-module(p10).

-include("tests_inc.hrl").

-export([encode/1]).

encode(Xs) ->
    encode(Xs, []).

encode([X | Xs], [{N, X} | Acc]) ->
    encode(Xs, [{N + 1, X} | Acc]);
encode([X | Xs], Acc) ->
    encode(Xs, [{1, X} | Acc]);
encode([], Acc) ->
    p05:reverse(Acc).

-ifdef(TEST).

encode_test_() -> [
    {"encode(42) throws exception",
        ?_assertException(error, function_clause, encode(42))},
    {"encode([]) is []",
        ?_assertEqual([], encode([]))},
    {"encode([a,a,a,a,b,c,c,a,a,d,e,e,e,e]) is [{4,a},{1,b},{2,c},{2,a},{1,d},{4,e}]",
        ?_assertEqual([{4,a},{1,b},{2,c},{2,a},{1,d},{4,e}],
            encode([a,a,a,a,b,c,c,a,a,d,e,e,e,e]))}
].

-endif.
