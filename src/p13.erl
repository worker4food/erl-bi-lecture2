-module(p13).

-include("tests_inc.hrl").

-export([decode/1]).

decode(Xs) ->
    decode(Xs, []).

decode([{1, X} | Xs], Acc) ->
    decode(Xs, [X | Acc]);
decode([{N, X} | Xs], Acc) ->
    decode([{N - 1, X} | Xs], [X | Acc]);
decode([], Acc) ->
    p05:reverse(Acc).

-ifdef(TEST).

decode_test_() -> [
    {"decode(42) throws exception",
        ?_assertException(error, function_clause, decode(42))},
    {"decode([]) is []",
        ?_assertEqual([], decode([]))},
    {"decode([{4,a},{1,b},{2,c},{2,a},{1,d},{4,e}]) is [a,a,a,a,b,c,c,a,a,d,e,e,e,e]",
        ?_assertEqual([a,a,a,a,b,c,c,a,a,d,e,e,e,e],
            decode([{4,a},{1,b},{2,c},{2,a},{1,d},{4,e}]))}
].

-endif.
