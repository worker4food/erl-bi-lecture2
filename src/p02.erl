-module(p02).

-include("tests_inc.hrl").

-export([but_last/1]).

but_last([_, _] = L) ->
    L;
but_last([_ | T]) ->
    but_last(T).

-ifdef(TEST).

but_last_test_() -> [
    {"but_last(42) throws exception",
        ?_assertException(error, function_clause, but_last(42))},
    {"but_last([]) throws exception",
        ?_assertException(error, function_clause, but_last([]))},
    {"but_last([42]) throws exception",
        ?_assertException(error, function_clause, but_last([42]))},
    {"but_last([a,b,c,d,e,f]) is [e, f]",
        ?_assertEqual([e, f], but_last([a,b,c,d,e,f]))},
    {"but_last(lists:seq(1, 100_000)) is [99_999, 100_000]",
        ?_assertEqual([99999, 100000], but_last(lists:seq(1, 100000)))},
    {"forall Xs, len(Xs) >= 2. length(but_last(Xs)) =:= 2",
        ?_assert(proper:quickcheck(prop_but_last_len_is_2()))},
    {"forall Xs, len(Xs) >= 2. but_last(Xs) returns suffix of Xs",
        ?_assert(proper:quickcheck(prop_but_last_is_suffix()))}
].

prop_but_last_is_suffix() ->
    ?FORALL(Xs, list2(any()),
        begin
            S = but_last(Xs),
            lists:suffix(S, Xs)
        end).

prop_but_last_len_is_2() ->
    ?FORALL(Xs, list2(any()), length(but_last(Xs)) =:= 2).

%% list with at least 2 elements
list2(Ty) ->
    ?LET({X1, Xs}, {Ty, non_empty(list(Ty))}, [X1 | Xs]).

-endif.
