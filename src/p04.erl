-module(p04).

-include("tests_inc.hrl").

-export([len/1]).

len(Xs) ->
    len(Xs, 0).

len([_ | Xs], Len) ->
    len(Xs, Len + 1);
len([], Len) ->
    Len.

-ifdef(TEST).

len_test_() -> [
    {"len(42) throws exception",
        ?_assertException(error, function_clause, len(42))},
    {"len([]) is 0",
        ?_assertEqual(0, len([]))},
    {"len([a,b,c,d]) is 4",
        ?_assertEqual(4, len([a,b,c,d]))},
    {"len([a, b, c] is 3",
        ?_assertEqual(3, len([a, b, c]))},
    {"forall Xs. len(Xs) =:= erlang:length(Xs)",
        ?_assert(proper:quickcheck(prop_len_eq_internal()))}
].

prop_len_eq_internal() ->
    ?FORALL(Xs, list(any()), len(Xs) =:= length(Xs)).
-endif.
