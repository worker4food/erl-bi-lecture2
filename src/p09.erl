-module(p09).

-include("tests_inc.hrl").

-export([pack/1]).

pack(Xs) ->
    pack(Xs, []).

pack([X | Xs], [[X | _] = G | Acc]) ->
    pack(Xs, [[X | G] | Acc]);
pack([X | Xs], Acc) ->
    pack(Xs, [[X] | Acc]);
pack([], Acc) ->
    p05:reverse(Acc).

-ifdef(TEST).

pack_test_() -> [
    {"pack(42) throws exception",
        ?_assertException(error, function_clause, pack(42))},
    {"pack([]) is []",
        ?_assertEqual([], pack([]))},
    {"pack([a,a,a,a,b,c,c,a,a,d,e,e,e,e]) is [[a,a,a,a],[b],[c,c],[a,a],[d],[e,e,e,e]]",
        ?_assertEqual([[a,a,a,a],[b],[c,c],[a,a],[d],[e,e,e,e]],
            pack([a,a,a,a,b,c,c,a,a,d,e,e,e,e]))}
].

-endif.
