-module(p03).

-include("tests_inc.hrl").

-export([element_at/2]).

element_at([X | _], 1) ->
    X;
element_at([_ | T], N) ->
    element_at(T, N - 1);
element_at([], _) ->
    undefined.

-ifdef(TEST).

element_at_test_() -> [
    {"element_at(42, 1) throws exception",
        ?_assertException(error, function_clause, element_at(42, 1))},
    {"element_at([a,b,c,d,e,f], 4) is d",
        ?_assertEqual(d, element_at([a,b,c,d,e,f], 4))},
    {"element_at([1, 2, 3], 0) = undefined",
        ?_assertEqual(undefined, element_at([1, 2, 3], 0))},
    {"forall Xs, Ix, Ix <= size(Xs). element_at(Xs, Ix) =:= lists:nth(Ix, Xs)",
        ?_assert(proper:quickcheck(prop_eq_lists_nth()))},
    {"forall Xs, Ix, Ix > size(Xs). element_at(Xs, Ix) =:= undefined",
        ?_assert(proper:quickcheck(prop_undefined_after_end()))}
].

prop_eq_lists_nth() ->
    ?FORALL({Xs, Ix}, list_and_index(any(), 20),
        element_at(Xs, Ix) =:= lists:nth(Ix, Xs)).

prop_undefined_after_end() ->
    ?FORALL({Xs, Ix}, list_and_index_after(any(), 20),
        element_at(Xs, Ix) =:= undefined).

%% generator for pair {Xs :: list(), Ix :: size()}
%% Ix <= length(Xs)
list_and_index(Ty, MaxLen) ->
    ?LET(N, range(1, MaxLen),
        tuple([vector(N, Ty), range(1, N)])).

%% generator for pair {Xs :: list(), Ix :: size()}
%% Ix <= length(Xs)
list_and_index_after(Ty, MaxLen) ->
    ?LET(N, choose(1, MaxLen),
        tuple([vector(N, Ty), range(N + 1, inf)])).

-endif.
