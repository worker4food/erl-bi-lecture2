-module(p15).

-include("tests_inc.hrl").

-export([replicate/2]).

replicate(Xs, N) ->
    replicate(Xs, N, 0, []).

replicate([_ | Xs], N, N, Acc) ->
    replicate(Xs, N, 0, Acc);
replicate([X | _] = Xs, N, M, Acc) ->
    replicate(Xs, N, M + 1, [X | Acc]);
replicate([], _, _, Acc) ->
    p05:reverse(Acc).

-ifdef(TEST).

replicate_test_() -> [
    {"replicate(42, 0) throws exception",
        ?_assertException(error, function_clause, replicate(42, 0))},
    {"replicate([], 100) is []",
        ?_assertEqual([], replicate([], 100))},
    {"replicate([a,b,c], 0) is []",
        ?_assertEqual([], replicate([a,b,c], 0))},
    {"replicate([a,b,c], 3) is [a,a,a,b,b,b,c,c,c]",
        ?_assertEqual([a,a,a,b,b,b,c,c,c], replicate([a,b,c], 3))}
].

-endif.
