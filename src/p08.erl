-module(p08).

-include("tests_inc.hrl").

-export([compress/1]).

compress(Xs) ->
    compress(Xs, []).

compress([X | Xs], [X | _] = Acc) ->
    compress(Xs, Acc);
compress([X | Xs], Acc) ->
    compress(Xs, [X | Acc]);
compress([], Acc) ->
    p05:reverse(Acc).

-ifdef(TEST).

compress_test_() -> [
    {"compress(42) throws exception",
        ?_assertException(error, function_clause, compress(42))},
    {"compress([]) is []",
        ?_assertEqual([], compress([]))},
    {"compress([a,a,a,a,b,c,c,a,a,d,e,e,e,e]) is [a,b,c,a,d,e]",
        ?_assertEqual([a,b,c,a,d,e], compress([a,a,a,a,b,c,c,a,a,d,e,e,e,e]))}
].

-endif.
