-module(p14).

-include("tests_inc.hrl").

-export([duplicate/1]).

duplicate(Xs) ->
    duplicate(Xs, []).

duplicate([X | Xs], Acc) ->
    duplicate(Xs, [X, X | Acc]);
duplicate([], Acc) ->
    p05:reverse(Acc).

-ifdef(TEST).

duplicate_test_() -> [
    {"duplicate(42) throws exception",
        ?_assertException(error, function_clause, duplicate(42))},
    {"duplicate([]) is []",
        ?_assertEqual([], duplicate([]))},
    {"duplicate([a,b,c,c,d]) is [a,a,b,b,c,c,c,c,d,d]",
        ?_assertEqual([a,a,b,b,c,c,c,c,d,d], duplicate([a,b,c,c,d]))}
].

-endif.
