-module(p05).

-include("tests_inc.hrl").

-export([reverse/1]).

reverse(Xs) ->
    reverse(Xs, []).

reverse([X | Xs], Acc) ->
    reverse(Xs, [X | Acc]);
reverse([], Acc) ->
    Acc.

-ifdef(TEST).

reverse_test_() -> [
    {"reverse(42) throws exception",
        ?_assertException(error, function_clause, reverse(42))},
    {"reverse([]) is []",
        ?_assertEqual([], reverse([]))},
    {"reverse([1,2,3]) is [3, 2, 1]",
        ?_assertEqual([3, 2, 1], reverse([1,2,3]))},
    {"forall Xs. reverse(Xs) =:= lists:reverse(Xs)",
        ?_assert(proper:quickcheck(prop_eq_lists_reverse()))}
].

prop_eq_lists_reverse() ->
    ?FORALL(Xs, list(any), reverse(Xs) =:= lists:reverse(Xs)).

-endif.
