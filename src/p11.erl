-module(p11).

-include("tests_inc.hrl").

-export([encode_modified/1]).

encode_modified(Xs) ->
    encode_modified(Xs, []).

encode_modified([X | Xs], [X | Acc]) ->
    encode_modified(Xs, [{2, X} | Acc]);
encode_modified([X | Xs], [{N, X} | Acc]) ->
    encode_modified(Xs, [{N + 1, X} | Acc]);
encode_modified([X | Xs], Acc) ->
    encode_modified(Xs, [X | Acc]);
encode_modified([], Acc) ->
    p05:reverse(Acc).

-ifdef(TEST).

encode_modified_test_() -> [
    {"encode_modified(42) throws exception",
        ?_assertException(error, function_clause, encode_modified(42))},
    {"encode_modified([]) is []",
        ?_assertEqual([], encode_modified([]))},
    {"encode_modified([a,a,a,a,b,c,c,a,a,d,e,e,e,e]) is [{4,a},b,{2,c},{2,a},d,{4,e}]",
        ?_assertEqual([{4,a},b,{2,c},{2,a},d,{4,e}],
            encode_modified([a,a,a,a,b,c,c,a,a,d,e,e,e,e]))}
].

-endif.
